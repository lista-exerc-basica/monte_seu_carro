

public class Carro
{
    private String motor;
    private double preco; 
    private boolean arCondicionado, cambioAutomatico, alarme, pinturaEspecialComemorativa, tetoSolar, kitMultimidia, rodaLiga, vidroAutomatico, ehImportado; 
    
    public Carro() {
        motor = "1.0";
    }
    
    public Carro(String motor, double preco, boolean arCondicionado, boolean cambioAutomatico, boolean alarme, boolean pinturaEspecialComemorativa, boolean tetoSolar, 
    boolean kitMultimidia, boolean rodaLiga, boolean vidroAutomatico, boolean ehImportado) {
        this.motor = motor;
        this.preco = preco;
        this.arCondicionado = arCondicionado;
        this.cambioAutomatico = cambioAutomatico;
        this.alarme = alarme;
        this.pinturaEspecialComemorativa = pinturaEspecialComemorativa;
        this.tetoSolar = tetoSolar;
        this.kitMultimidia = kitMultimidia;
        this.rodaLiga = rodaLiga;
        this.vidroAutomatico = vidroAutomatico;
        this.ehImportado = ehImportado;
    }
    
    private double taxaImportacao() {
        return ehImportado? preco * 0.3 : 0;   
    }
    
    private double precoCambio() {
        return cambioAutomatico? 5000 : 0;
    }
    
    private double precoArCondicionado() {
        return arCondicionado? 3000 : 0;   
    }
    
    private double precoAlarme() {
        return cambioAutomatico? 800 : 0;   
    }
    
    private double precoPintura() {
        return pinturaEspecialComemorativa? 2500 : 0;   
    }
    
    private double precoTeto() {
        return tetoSolar? 4000 : 0;
    }
    
    private double precoKitMultimidia() {
        return kitMultimidia? 1800 : 0;    
    }
    
    private double precoRodaLiga() {
        return rodaLiga? 3000 : 0;    
    }
    
    private double precoVidro() {
        return vidroAutomatico? 800 : 0;  
    }
    
    private double ipi(double valor) {
        return motor.equals("1.0")? 0.1 * valor : 0.2 * valor;    
    }
    
    public double custo(){
        double precoTotal = this.preco + taxaImportacao() + precoArCondicionado() + precoCambio() + precoAlarme() + precoPintura() + precoTeto() + precoKitMultimidia() + precoRodaLiga()
         + precoVidro();
        
        return precoTotal + ipi(precoTotal);   
    }
    
    public void setPreco(double preco) {
        this.preco = preco;   
    }
    
    public double getPreco() {
        return this.preco;   
    }
    
    public void setMotor(String motor) {
            this.motor = motor;  
    }
    
    public String getMotor() {
        return this.motor;   
    }
    
    public void setArCondicionado(boolean arCondicionado) {
        this.arCondicionado = arCondicionado;   
    }
    
    public boolean getArCondicionado() {
        return this.arCondicionado;
    }
    
    public void setCambioAutomatico (boolean cambioAutomatico) {
        this.cambioAutomatico = cambioAutomatico;   
    }
    
    public boolean getCambioAutomatico() {
        return this.cambioAutomatico;   
    }
    
    public void setAlarme(boolean alarme) {
        this.alarme = alarme;   
    }
    
    public boolean getAlarme() {
        return this.alarme;   
    }
    
    public void setPinturaEspecialComemorativa(boolean pinturaEspecialComemorativa) {
        this.pinturaEspecialComemorativa = pinturaEspecialComemorativa;   
    }
    
    public boolean getPinturaEspecialComemorativa() {
        return pinturaEspecialComemorativa;   
    }
    
    public void setTetoSolar(boolean tetoSolar) {
        this.tetoSolar = tetoSolar;   
    }
    
    public boolean getTetoSolar() {
        return this.tetoSolar;   
    }
    
    public void setKitMultimidia (boolean kitMultimidia) {
        this.kitMultimidia = kitMultimidia;   
    }
    
    public void setRodaLiga(boolean rodaLiga) {
        this.rodaLiga = rodaLiga;   
    }
    
    public boolean getRodaLiga() {
        return this.rodaLiga;   
    }
    
    public void setVidroAutomatico(boolean vidroAutomatico) {
        this.vidroAutomatico = vidroAutomatico;   
    }
    
    public boolean getVidroAutomatico() {
        return this.vidroAutomatico;   
    }
}
    
    


import java.util.Scanner;

public class Main
{
    public static void main (String[] args) {
        
        Scanner le = new Scanner(System.in);
        
        Carro c = new Carro();
        
        System.out.print("Informe o valor base do carro: ");
        c.setPreco(le.nextDouble());
        
        System.out.print("Informe a cilindrada do motor: ");
        c.setMotor(le.next());
        
        System.out.println("O carro possui câmbio automático? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setCambioAutomatico(false);   
        } else {
            c.setCambioAutomatico(true);   
        }
        
        System.out.println("O carro possui câmbio teto solar? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setTetoSolar(false);   
        } else {
            c.setTetoSolar(true);   
        }
        
        System.out.println("O carro possui câmbio ar condicionado? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setArCondicionado(false);   
        } else {
            c.setArCondicionado(true);   
        }
        
        System.out.println("O carro possui alarme? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setAlarme(false);   
        } else {
            c.setAlarme(true);   
        }
        
        System.out.println("O carro possui pintura especial comemorativa? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setPinturaEspecialComemorativa(false);   
        } else {
            c.setPinturaEspecialComemorativa(true);   
        }
        
        System.out.println("O carro possui roda de liga leve? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setRodaLiga(false);   
        } else {
            c.setRodaLiga(true);   
        }
        
        System.out.println("O carro possui kit multimidia? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setKitMultimidia(false);   
        } else {
            c.setKitMultimidia(true);   
        }
        
        System.out.println("O carro possui vidro automatico? (S)im/(N)ão");
        if(le.next().equalsIgnoreCase("N")) {
            c.setVidroAutomatico(false);   
        } else {
            c.setVidroAutomatico(true);   
        }
        
        System.out.println("O valor final do veículo é R$" + c.custo());
    }
}
